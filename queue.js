let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

    if (collection.length == 0) {
        return [];
    } else {
        return collection;
    }

}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method

    collection[collection.length] = element;

    return collection;

}

function dequeue() {
    // In here you are going to remove the last element in the array

    let newCollection = []
    for (let index = 1; index < collection.length; index++) {
        newCollection[newCollection.length] = collection[index];
    }

    collection = newCollection

    return collection;

}

function front() {
    // In here, you are going to remove the first element
    let firstElement = collection[0];

    return firstElement;
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
     
     let count = 0

     for (let index = 0; index < collection.length; index++) {
        if (collection[index] !== null) {
            count++;
        }
     }

     return count;
     
}

function isEmpty() {
    //it will check whether the function is empty or not

    if (collection.length == 0) {
        return true;
    } else {
        return false;
    }
    
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};